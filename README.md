# OpenTable - (MeryLo)
Práctica de HTML y CSS.

## Descripción
Este repositorio contiene la maquetación de práctica de un footer para una página web, basado en http://www.opentable.com
Está hecho con HTML y CSS plano.

## Screenshot
![Screenshot de la aplicación](https://gitlab.com/tr11.wfsdft.mar19/opentable-mg/raw/master/screenshot.PNG)

## License

  [MIT](LICENSE)